(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("02f57ef0a20b7f61adce51445b68b2a7e832648ce2e7efb19d217b6454c1b644" "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" "1704976a1797342a1b4ea7a75bdbb3be1569f4619134341bd5a4c1cfb16abad4" "1d5e33500bc9548f800f9e248b57d1b2a9ecde79cb40c0b1398dec51ee820daf" default))
 '(delete-selection-mode nil)
 '(package-selected-packages '(neotree mixed-pitch gradle-mode))
 '(safe-local-variable-values
   '((org-latex-hyperref-template)
     (org-publish-project-alist
      ("thesis" :base-directory "~/projects/bacc-thesis/writeup/src" :publishing-function #'org-latex-publish-to-latex :publishing-directory "~/projects/bacc-thesis/writeup/build" :with-toc nil))
     (org-publish-project-alist
      ("thesis" :base-directory "~/projects/bacc-thesis/writeup/src" :publishing-function #'org-latex-publish-to-latex :publishing-directory "~/projects/bacc-thesis/writeup/src" :with-toc nil))
     (org-latex-classes
      ("scrreprt" "\\documentclass[11pt]{scrreprt} [EXTRA]"
       ("\\chapter{%s}" . "\\chapter*{%s}")
       ("\\section{%s}" . "\\section*{%s}")
       ("\\subsection{%s}" . "\\subsection*{%s}")
       ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
       ("\\paragraph{%s}" . "\\paragraph*{%s}")))
     (org-latex-with-hyperref)
     (org-latex-default-packages-alist)
     (org-latex-classes
      ("scrreprt" "\\documentclass[11pt]{scrreprt} [EXTRA]"
       ("\\chapter{%s}" . "\\chapter*{%s}")
       ("\\section{%s}" . "\\section*{%s}")
       ("\\subsection{%s}" . "\\subsection*{%s}")
       ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
